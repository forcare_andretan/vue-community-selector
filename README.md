# vue-community-selector

## Project setup
```
npm install
```

## build webcomponent
`npx vue-cli-service build --target wc --name community-selector ./src/components/CommunitySelector.vue`

## build webcomponent including vue
`npx vue-cli-service build --inline-vue --target wc --name community-selector ./src/components/CommunitySelector.vue`


## publish to NPM
`npm publish`

## Bump version
`npm version patch`

# How to reference the webcomponent
The community-selector requires a URL attribute to retrieve the FHIR Organization resource list that represents the organizations in a community. 
`<community-selector url="/viewer/services/organizations.json" />`
## Listening to events
The community-selector dispatches two events, "ok" and "cancel". There are normal dom events and can be accessed in forView using Dojo the following way:
```
on(communitySelectorNode, 'ok', function(evt) {
  // access the selected documents in evt.detail.data.selectedDocs
});
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your unit tests
```
npm run test:unit
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
